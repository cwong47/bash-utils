#!/usr/bin/env bash
#
#  Author      : Calvin Wong
#  Date        : 20221209
#  What        : Manipulate Git tags.
#  Source      : https://stackoverflow.com/questions/49655467/shell-script-bump-version-automatically-git
#                https://stackoverflow.com/questions/3867619/how-to-get-last-git-tag-matching-regex-criteria

set -euo pipefail

fn_usage() {
    local exit="${1:-1}"

    echo "Usage: ${0##*/} [-h | -l] [-b #minor|#patch] ['pattern*']" 1>&2
    echo "       -h                     Print help." 1>&2
    echo "       -l                     List tags." 1>&2
    echo "       -b #minor|#patch       Bump the #minor or #patch." 1>&2
    exit "$exit"
}

while getopts ":b:lh" opt; do
    case "${opt}" in
        h)
            fn_usage 0
            ;;
        l)
            opt_action="list"
            ;;
        b)
            opt_action="bump"
            case "${OPTARG}" in
                minor)
                    opt_bump=1
                    ;;
                patch)
                    opt_bump=0
                    ;;
                *)
                    fn_usage 3
                    ;;
            esac
            ;;
        :)
            echo "Option -$OPTARG requires an argument ..." 1>&2
            fn_usage 1
            ;;
    esac
done
shift $((OPTIND-1))

opt_pattern="${1:-*}"

case "${opt_action:-list}" in
    list)
        git tag --list --sort=-version:refname "$opt_pattern"
        ;;
    bump)
        git describe --tags --match "${opt_pattern}v[0-9]*" --abbrev=0 HEAD | awk -v b="${opt_bump:-}" -F"." '{OFS="."; $(NF-b)+=1; print $0}'
        ;;
esac
