# bash prompt utils

Collection of tools to beautify the bash prompt! :P

My current bash prompt using the following scripts.

```
──── ✓ ─── user@hostname ⠕ prod-us-gke-proj/prod-cluster-c01/default ─── Sun Oct 04 ･ 08:06 AM ─── ~/src/.../dev-us-east-1/.terraform ‹master› ─────────
────
```

**ps1-git-status**

```
$ ps1-git-status -h
Usage: ps1-git-status [-h | -? | --help] [--no-color] [-c | --color-output Color] [-t | --color-tainted Color]
       -h | -? | --help                  Print help.
       --no-color                        Disable color output.
       -c | --color-output "\033[96;1m"  Output color.
       -t | --color-tainted "\033[91;1m" Color of the tainted branch.

$ ps1-git-status
‹master ᓰ›

$ ps1-git-status
‹941c86ebc77402f9623cb5b3989244bcebcb69c6›
```

**ps1-kube-context**

```
$ ps1-kube-context --no-color
prod-us-gke-proj/prod-cluster-c01/default
```

**ps1-pwd**

```
$ tput cols
181

$ pwd
/Users/cwong/Downloads/youtube-videos
$ ps1-pwd
~/Downloads/youtube-videos

$ pwd
/Users/cwong/src/github.com/Work-Stuff/techops-tools/terraform/environments/dev-us-east-1/.terraform
$ ps1-pwd
~/src/.../dev-us-east-1/.terraform
```
