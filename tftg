#!/usr/bin/env bash
#
#  Author      : Calvin Wong
#  Date        : 20191120
#  What        : Parse Terraform files and display target outputs.
#  Requirement : terraform

set -euo pipefail

fn_usage() {
    local exit="${1:-1}"

    echo "Usage: ${0##*/} [-h | -1] [-i refresh|plan|apply|destroy] terraform.tf ..." 1>&2
    echo "       -h                     Print help." 1>&2
    echo "       -1                     Display one resource/module per line." 1>&2
    echo "       -i [action]            Interactive mode." 1>&2
    exit "$exit"
}

fn_tftarget() {
    local tf_files="${@:-*.tf}"

    grep -E "^\s*(module|resource)\s*\"" $tf_files 2> /dev/null \
        | cut -d: -f2- \
        | while read -r line; do
            set -- $line
            (([[ "$1" == "module" ]] && echo "$1.$2") || ([[ "$1" == "resource" ]] && echo "$2.$3")) \
                | tr -d '"'
        done
}

while getopts ":i:1h" opt; do
    case "${opt}" in
        h)
            fn_usage 0
            ;;
        1)
            opt_oneliner="\n"
            ;;
        i)
            opt_action="${OPTARG}"
            ;;
        *)
            fn_usage 1
            ;;
    esac
done
shift $((OPTIND-1))

if [[ -z "$*" && -z "${opt_action:-}" ]]; then
    fn_usage 1
else
    case "${opt_action:-}" in
        refresh|plan|apply|destroy)
            list=""
            targets=$(fn_tftarget "$@" \
                | sort -r \
                | fzf \
                    --header="Press ESC to exit, TAB to select" \
                    --height=25% \
                    --layout=reverse \
                    --multi \
                    --prompt="Select target(s) to ${opt_action}› ")
            if [[ -n "${targets:-}" ]]; then
                for target in $targets; do
                    list="$list -target $target "
                done
            fi
            [[ -n "${list:-}" ]] && terraform ${opt_action} ${list};
            ;;
        *)
            fn_tftarget "$@" | sed 's/^/-target /' | tr "\n" "${opt_oneliner:- }"
            [[ -z "${opt_oneliner:-}" ]] && echo
            ;;
    esac
fi
