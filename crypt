#!/usr/bin/env bash
#
#  Author      : Calvin Wong
#  Date        : 20160806
#  What        : Encrypt/Decrypt file using openssl aes256 or 7-zip.
#  Source      : http://www.tecmint.com/encrypt-decrypt-files-tar-openssl-linux/
#                http://www.tecmint.com/linux-password-protect-files-with-encryption/
#  Requirement : openssl, 7-zip

set -euo pipefail

fn_usage() {
    local exit="${1:-1}"

    echo "Usage: ${0##*/} [-e|-d|-7] [filename|filename.(aes|7z)]" 1>&2
    exit "$exit"
}

[[ "${1:-}" != "-e" && "${1:-}" != "-d" && "${1:-}" != "-7" ]] && fn_usage 1
[[ -z "${2:-}" ]] && fn_usage 2
[[ "${1:-}" == "-e" ]] && outfile="${2}.aes"
[[ "${1:-}" == "-d" ]] && outfile="${2/.aes}"

case "$1" in
    -7)
        if [[ "$2" =~ .7z$ ]]; then
            7za e "$2"
        else
            7za a -p -mx=9 -mhe -t7z "${2}.7z" "$2"
        fi
        ;;
    -e|-d)
        openssl enc "$1" -aes256 -salt -in "$2" -out "$outfile" 2> /dev/null
        ;;
esac
