# bash-utils

Collection of tools to make bash'ing more fun! ;P

## Tools migrated from my bash functions

- **crypt**
    - Encrypt/Decrypt file using openssl aes256 or 7-zip.
    - *Best to just use 7-zip option because each version of openssl acts differently.*
- **hl**
    - Highlight matched pattern(s).
- **ht**
    - Display head and tail of a file.
- **randpw**
    - Generate random password/token.
-  **simpleshare**
    - Simple http server using python.
- **tftg**
    - Parse Terraform files and display target outputs.
-  **url-speedtest**
    - Website responds time check.

## Tools migrated from private script repo that are semi-presentable :P

- **color**
    - It displays all possible colors under a UNIX terminal emulator.
- **randcap**
    - Randomly capicalize a given string.
- **randcol**
    - Randomly colorize a given string.
- **github-users**
    - Get Github users from given organization.
- **slack-users**
    - Get Slack users from given channel.
- **tag**
    - Manipulate Git tags: list/bump (#minor/#patch).
